package santos.bruno.ifood.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import santos.bruno.ifood.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
